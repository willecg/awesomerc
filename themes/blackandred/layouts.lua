local awful = require("awful")
awful.util = require("awful.util")

theme_path = awful.util.getdir("config") .. "/themes/blackandred/"

theme.layout_fairh = theme_path.."layouts/fairh.png"
theme.layout_fairv = theme_path.."layouts/fairv.png"
theme.layout_floating  = theme_path.."layouts/floating.png"
theme.layout_magnifier = theme_path.."layouts/magnifier.png"
theme.layout_max = theme_path.."layouts/max.png"
theme.layout_fullscreen = theme_path.."layouts/fullscreen.png"
theme.layout_tilebottom = theme_path.."layouts/tilebottom.png"
theme.layout_tileleft   = theme_path.."layouts/tileleft.png"
theme.layout_tile = theme_path.."layouts/tileright.png"
theme.layout_tiletop = theme_path.."layouts/tiletop.png"
theme.layout_spiral  = theme_path.."layouts/spiral.png"
theme.layout_dwindle = theme_path.."layouts/dwindle.png"
theme.layout_cornernw = theme_path.."layouts/cornernw.png"
theme.layout_cornerne = theme_path.."layouts/cornerne.png"
theme.layout_cornersw = theme_path.."layouts/cornersw.png"
theme.layout_cornerse = theme_path.."layouts/cornerse.png"

