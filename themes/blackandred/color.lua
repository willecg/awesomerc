local _M = {}

_M.color = {

  ["black"] = "#241f31",
  ["blackl"] = "#9a9996",
  ["red"] = "#a51d2d",
  ["redl"] = "#f66151",
  ["green"] = "#26a269",
  ["greenl"] = "#8ff0a4",
  ["yellow"] = "#e5a50a",
  ["yellowl"] = "#f9f06b",
  ["blue"] = "#1a5fb4",
  ["bluel"] = "#99c1f1",
  ["magenta"] = "#613583",
  ["magental"] = "#dc8add",
  ["cyan"] = "#589082",
  ["cyanl"] = "#74aa98",
  ["white"] = "#deddda",
  ["whitel"] = "#f6f5f4",
  ["brown"] = "#63452c",
  ["brownl"] = "#cbab8f"
}

return _M
