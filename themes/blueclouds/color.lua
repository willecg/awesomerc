local _M = {}

_M.color = {
  ["black"] = "#1a2033",
  ["blackl"] = "#3a4771",
  ["red"] = "#8c1c13",
  ["redl"] = "#eb786f",
  ["green"] = "#60993e",
  ["greenl"] = "#77b851",
  ["yellow"] = "#f3b61f",
  ["yellowl"] = "#f9da8b",
  ["blue"] = "#4f86c6",
  ["bluel"] = "#74a0d2",
  ["magenta"] = "#a755c2",
  ["magental"] = "#c187d4",
  ["cyan"] = "#589082",
  ["cyanl"] = "#74aa98",
  ["white"] = "#a3a3a3",
  ["whitel"] = "#f6f6f6"
}

return _M
