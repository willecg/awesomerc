local color = require("themes.blueclouds.color")
local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gears = require("gears")
shape = gears.shape

local awful = require("awful")
awful.util = require("awful.util")
local theme_path = awful.util.getdir("config") .. "themes/blueclouds/"
theme.font          = "Cantarell 9"

theme.bg_normal     = color.color["black"]
theme.bg_focus      = color.color["blackl"]
theme.bg_urgent     = color.color["red"]
theme.bg_minimize   = theme.bg_normal
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = color.color["whitel"]
theme.fg_focus      = color.color["whitel"]
theme.fg_urgent     = color.color["whitel"]
theme.fg_minimize   = color.color["white"]

theme.useless_gap   = dpi(1)
theme.border_width  = dpi(1)
theme.border_normal = color.color["black"]
theme.border_focus  = color.color["white"]
theme.border_marked = color.color["blue"]
theme.wibar_height = dpi(20)
theme.wibar_bg = color.color["blackl"]
-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
theme.taglist_bg_empty = theme.bg_normal
theme.taglist_bg_occupied = theme.bg_normal
theme.tasklist_bg = theme.bg_normal
theme.tasklist_bg_focus = color.color["black"]
theme.tasklist_fg_focus = color.color["greenl"]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
theme.hotkeys_modifiers_fg = color.color["green"]

-- Example:
--theme.taglist_bg_focus = "#ff0000"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
     taglist_square_size, theme.fg_normal
 )

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]
--theme.notification_bg = theme.bg_focus
--theme.notification_border_width = 4
--theme.notification_border_color = '#f6f6f6'
-- theme.notification_shape = function(cr, w, h)
--   gears.shape.infobubble(cr, w, h)
-- end

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]


theme.menu_submenu_icon = theme_path.."submenu.png"
theme.menu_height = dpi(20)
theme.menu_width  = dpi(150)

theme.bat = theme_path .. "widgets/bat.png"
theme.bat_ac = theme_path .. "widgets/bat_ac.png"
theme.bat_critical = theme_path .. "widgets/bat_critical.png"
theme.bat_full = theme_path .. "widgets/bat_full.png"
theme.cpu = theme_path .. "widgets/cpu.png"
theme.filesystem = theme_path .. "widgets/filesystem.png"
theme.mail = theme_path .. "widgets/mail.png"
theme.mail_new = theme_path .. "widgets/mail_new.png"
theme.mpd = theme_path .. "widgets/mpd.png"
theme.ram = theme_path .. "widgets/ram.png"
theme.temp = theme_path .. "widgets/temp.png"
theme.todo = theme_path .. "widgets/todo.png"
theme.up_down = theme_path .. "widgets/up_down.png"
theme.vol_high = theme_path .. "widgets/vol_high.png"
theme.vol_low = theme_path .. "widgets/vol_low.png"
theme.vol_mid = theme_path .. "widgets/vol_mid.png"
theme.vol_mute = theme_path .. "widgets/vol_mute.png"
theme.weather = theme_path .. "widgets/weather.png"
theme.wifi = theme_path .. "widgets/wifi.png"

