local gfs = require("gears.filesystem")
local awful = require("awful")
awful.util = require("awful.util")

theme_path = awful.util.getdir("config") .. "themes/blueclouds/"

theme.titlebar_close_button_normal = theme_path.."titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme_path.."titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme_path.."titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme_path.."titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme_path.."titlebar/ontop_normal.png"
theme.titlebar_ontop_button_focus_inactive  = theme_path.."titlebar/ontop_focus.png"
theme.titlebar_ontop_button_normal_active = theme_path.."titlebar/ontop_active_normal.png"
theme.titlebar_ontop_button_focus_active  = theme_path.."titlebar/ontop_active_focus.png"

theme.titlebar_sticky_button_normal_inactive = theme_path.."titlebar/sticky_normal.png"
theme.titlebar_sticky_button_focus_inactive  = theme_path.."titlebar/sticky_focus.png"
theme.titlebar_sticky_button_normal_active = theme_path.."titlebar/sticky_active_normal.png"
theme.titlebar_sticky_button_focus_active  = theme_path.."titlebar/sticky_active_focus.png"

theme.titlebar_floating_button_normal_inactive = theme_path.."titlebar/floating_normal.png"
theme.titlebar_floating_button_focus_inactive  = theme_path.."titlebar/floating_focus.png"
theme.titlebar_floating_button_normal_active = theme_path.."titlebar/floating_active_normal.png"
theme.titlebar_floating_button_focus_active  = theme_path.."titlebar/floating_active_focus.png"

theme.titlebar_maximized_button_normal_inactive = theme_path.."titlebar/maximize_normal.png"
theme.titlebar_maximized_button_focus_inactive  = theme_path.."titlebar/maximize_focus.png"
theme.titlebar_maximized_button_normal_active = theme_path.."titlebar/maximize_active_normal.png"
theme.titlebar_maximized_button_focus_active  = theme_path.."titlebar/maximize_active_focus.png"

