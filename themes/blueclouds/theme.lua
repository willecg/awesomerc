local awful = require("awful")
awful.util = require("awful.util")


theme_path = awful.util.getdir("config") .. "themes/blueclouds/"
theme = {}

dofile(theme_path .. "elements.lua")
dofile(theme_path .. "titlebar.lua")
dofile(theme_path .. "layouts.lua")

--theme.wallpaper = theme_path .. "background.jpg"
theme.awesome_icon = theme_path .. "launcher/icon.png"
theme.awesome_subicon = theme_path .. "launcher/icon.png"

return theme
