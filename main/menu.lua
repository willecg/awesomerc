local awful = require("awful")
local hotkeys_popup = require("awful.hotkeys_popup").widget
local beautiful = require("beautiful")

local M = {}
local _M = {}

local terminal = "urxvtc"
local editor = "emacs"
local editor_cmd = "emacs"

function _M.get()
  local menu_item = {
    { "awesome", M.awesome, beautiful.awesome_subicon },
    { "open terminal", terminal }
  }
  return menu_item
end

return setmetatable(
  {},
  { __call = function(_, ...) return _M.get(...) end }
)
