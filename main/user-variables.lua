local home = os.getenv("HOME")

local _M = {
  -- terminal
  terminal = "urxvtc",
  editor = "emacs" or os.getenv("EDITOR"),
  editor_cmd = editor,
  modkey = "Mod4",
}
return _M
