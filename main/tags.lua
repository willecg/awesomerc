local awful = require("awful")

local _M = {}

function _M.get ()
  tags = {}

  awful.screen.connect_for_each_screen(function(s)
      -- Each screen has its own tag table.
      tags[s] = awful.tag(
        { "①", "②", "③", "④", "⑤", "⑥", "⑦", "⑧", "⑨" }, s, RC.layouts[1])
  end)
  return tags
end

return setmetatable(
  {},
  { __call = function(_, ...) return _M.get(...) end}
)
