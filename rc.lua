-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local gears = require("gears")
local awful = require("awful")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
local menubar = require("menubar")
local hotkeys_popup = require("awful.hotkeys_popup")

local cf_path = awful.util.getdir("config")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

require("main.error-handling")
RC = {}
RC.vars = require("main.user-variables")

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(cf_path .. "themes/blackandred/theme.lua")
RC.default_bg = beautiful.bg_normal

-- Custom local library
local main = {
  layouts = require("main.layouts"),
  tags    = require("main.tags"),
  menu    = require("main.menu"),
  rules   = require("main.rules"),
}
-- custom local library: Keys and mouse bindings
local binding = {
  globalbuttons = require("binding.globalbuttons"),
  clientbuttons = require("binding.clientbuttons"),
  globalkeys    = require("binding.globalkeys"),
  bindtotags    = require("binding.bindtotags"),
  clientkeys    = require("binding.clientkeys")
}

RC.layouts = main.layouts()
RC.tags = main.tags()
RC.mainmenu = awful.menu({ items = main.menu() })
RC.launcher = awful.widget.launcher(
  { image = beautiful.awesome_icon, menu = RC.mainmenu }
)
menubar.utils.terminal = RC.vars.terminal

-- Mouse and key bindings
RC.globalkeys = binding.globalkeys()
RC.globalkeys = binding.bindtotags(RC.globalkeys)

-- set root
root.buttons(binding.globalbuttons())
root.keys(RC.globalkeys)

require("deco.statusbar")

-- rules
awful.rules.rules = main.rules(
  binding.clientkeys(),
  binding.clientbuttons())

-- signals
require("main.signals")

-- autoruun
local gears = {
  table = require("gears.table"),
  p_call = require("gears.protected_call")
}
gears.p_call( dofile, cf_path .. "main/autorun.lua")
