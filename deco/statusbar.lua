-- Standard awesome library
local gears = require("gears")
local awful = require("awful")

local wibox = require("wibox")

os.setlocale(os.getenv("LANG"))
local lain = require("lain")
local separators = lain.util.separators
local markup = lain.util.markup

-- custom local library: common functional decoration
local deco = {
  wallpaper = require("deco.wallpaper"),
  taglist = require("deco.taglist"),
  tasklist = require("deco.tasklist")
}

local beautiful = require("beautiful")

local taglist_buttons = deco.taglist()
local tasklist_buttons = deco.tasklist()

local _M = {}

-- wibar
-- Keyboard map indicator and switcher
mykeyboardlayout = awful.widget.keyboardlayout()
lk_background = wibox.widget{
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
    mykeyboardlayout,
    bg=beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- Create a textclock widget
mytextclock = wibox.widget.textclock(" <b>%A, %d de %B del %Y %I:%M</b> ")
tc_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
    mytextclock,
    bg = beautiful.bg_normal,
    widget = wibox.container.background,
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- Calendario
local calendario = lain.widget.cal({
    attach_to = {mytextclock},
    notification_preset = {
      font = "Monospace 10",
      fg = beautiful.fg_normal,
      bg = beautiful.bg_normal,
      position = "top_right"
    }
})

-- cpu widget
local i_cpu = wibox.widget.imagebox(beautiful.cpu)
local cpu = lain.widget.cpu {
  settings = function()
    widget:set_markup(" "..cpu_now.usage .. "% ")
  end
}
cpu_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(i_cpu, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    cpu.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- mem widget
local i_ram = wibox.widget.imagebox(beautiful.ram)
local ram = lain.widget.mem {
  settings = function()
    widget:set_markup(" " .. mem_now.perc .. "% ")
  end
}
ram_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(i_ram, 2, 2, 4),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    ram.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- net widget
local i_net = wibox.widget.imagebox(beautiful.wifi)
local net = lain.widget.net {
  iface = "eth0",
  settings = function()
    widget:set_markup(" " .. net_now.received .. " ↓↑ ".. net_now.sent .. " ")
  end
}
net_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
    wibox.container.margin(i_net, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    net.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- widget de batería
i_bat = wibox.widget.imagebox(beautiful.bat)
bat = lain.widget.bat({
      timeout = 10,
      battery="BAT0",
      settings = function()
	 if bat_now.status == "Charging" then
	    i_bat:set_image(beautiful.bat_ac)
	    widget:set_markup(" " .. tonumber(bat_now.perc) .. "% ")
	 elseif bat_now.status == "Discharging" then
	    widget:set_markup(" " .. tonumber(bat_now.perc) .. "% ")
	    if tonumber(bat_now.perc) <= 15 then
	       i_bat:set_image(beautiful.bat_critical)
	    elseif tonumber(bat_now.perc) <= 90 then
	       i_bat:set_image(beautiful.bat)
	    else
	       i_bat:set_image(beautiful.bat_full)
	    end
	 elseif bat_now.status == "Full" then
	    widget:set_markup(" Cargado ")
	 end
      end
})

bat_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
    wibox.container.margin(i_bat, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    bat.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- vol widget
local i_vol = wibox.widget.imagebox(beautiful.vol_high)
local vol = lain.widget.alsa({
    settings = function()
        if volume_now.status == "off" then
            i_vol:set_image(beautiful.vol_mute)
        elseif tonumber(volume_now.level) <= 20 then
            i_vol:set_image(beautiful.vol_low)
        elseif tonumber(volume_now.level) <= 75 then
            i_vol:set_image(beautiful.vol_mid)
        else
            i_vol:set_image(theme.vol_high)
        end

        widget:set_markup(" " .. volume_now.level .. "% ")
    end
})

vol.widget:buttons(awful.util.table.join(
                               awful.button({}, 4, function ()
                                     awful.util.spawn("amixer set Master 1%+")
                                     vol.update()
                               end),
                               awful.button({}, 5, function ()
                                     awful.util.spawn("amixer set Master 1%-")
                                     vol.update()
                               end)
))

vol_background = wibox.widget {
  separators.arrow_left("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(i_vol, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    vol.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_left(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- weather widget
local i_weather = wibox.widget.imagebox(beautiful.weather)
wth = lain.widget.weather({
    city_id = 4018404,
    APPID = "Your ID",
    lang = "es",
    cnt = 5,
    default_forecast_call = "curl -s 'https://api.openweathermap.org/data/2.5/forecast?id=%s&units=%s&lang=%s&cnt=%s'",
    notification_preset = {
      fg = beautiful.fg_normal,
      position = "top_left"
    },
    settings = function ()
      units = math.floor(weather_now["main"]["temp"])
      widget:set_markup(" " .. units .. "°C ")
    end
})

wea_background = wibox.widget {
  separators.arrow_right("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(wth.icon, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    wth.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_right(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- imap widget
local i_mail = wibox.widget.imagebox(beautiful.mail)
local mail = lain.widget.imap({
    server = "mail.tld",
    timeout = 1800,
    mail = "yourusermail@mail.tld",
    password = "YourPassword",
    settings = function ()
      if mailcount > 0 then
        i_mail:set_image(beautiful.mail_new)
        widget:set_markup(mailcount .. " ")
      else
        widget:set_markup("")
        i_mail:set_image(beautiful.mail)
      end
    end
})

mail_background = wibox.widget {
  separators.arrow_right("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(i_mail, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    mail.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_right(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

-- mpd widget
local i_mpd = wibox.widget.imagebox(beautiful.mpd)
local mpd = lain.widget.mpd({
    settings = function ()
      if mpd_now.state == "play" then
        widget:set_markup("> " .. mpd_now.artist .. " - " .. mpd_now.title)
      elseif mpd_now.state == "pause" then
        widget:set_markup("|| " .. mpd_now.artist .. " - " .. mpd_now.title)
      else
        widget:set_markup("")
      end
    end
})

local mpd_background = wibox.widget {
  separators.arrow_right("alpha", beautiful.bg_normal),
  {
     wibox.container.margin(i_mpd, 2, 2, 2, 2),
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  {
    mpd.widget,
    bg = beautiful.bg_normal,
    widget = wibox.container.background
  },
  separators.arrow_right(beautiful.bg_normal, "alpha"),
  layout = wibox.layout.fixed.horizontal
}

awful.screen.connect_for_each_screen(function(s)
    -- wallpaper
    set_wallpaper(s)

    -- create promptbox
    s.mypromptbox = awful.widget.prompt()

    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                            awful.button({ }, 1, function () awful.layout.inc( 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(-1) end),
                            awful.button({ }, 4, function () awful.layout.inc( 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    lb_background = wibox.widget {
       separators.arrow_left("alpha", beautiful.bg_normal),
      {
	 wibox.container.margin(s.mylayoutbox, 2, 2, 2, 2),
        bg = beautiful.bg_normal,
        widget = wibox.container.background
       },
      --separators.arrow_left(beautiful.bg_normal, "alpha"),
      layout = wibox.layout.fixed.horizontal
    }

    lch_background = wibox.widget {
     --separators.arrow_right("alpa", beautiful.bg_normal),
     {
        RC.launcher,
        bg = beautiful.bg_normal,
        margin = 4,
        widget = wibox.container.background
      },
      separators.arrow_right(beautiful.bg_normal, "alpha"),
      layout = wibox.layout.fixed.horizontal
    }

    st_background = wibox.widget {
      separators.arrow_left("alpha", beautiful.bg_normal),
      {
	 wibox.container.margin(wibox.widget.systray(), 2, 2, 2, 2),
        bg = beautiful.bg_normal,
        margin = 4,
        widget = wibox.container.background
      },
      separators.arrow_left(beautiful.bg_normal, "alpha"),
      layout = wibox.layout.fixed.horizontal
    }

    --create wibox
    s.mywibox = awful.wibar({ position = "top", screen = s })

        --create taglist widget
    s.mytaglist = awful.widget.taglist {
      screen  = s,
      filter  = awful.widget.taglist.filter.all,
      buttons = taglist_buttons,
      layout = wibox.layout.fixed.horizontal,
    }
    ltg_background = wibox.widget {
     separators.arrow_right("alpa", beautiful.bg_normal),
     {
        s.mytaglist,
        bg = beautiful.bg_normal,
        margin = 4,
        widget = wibox.container.background
      },
      separators.arrow_right(beautiful.bg_normal, "alpha"),
      layout = wibox.layout.fixed.horizontal
    }

    --create tasklist widget
    s.mytasklist = awful.widget.tasklist {
      screen  = s,
      filter  = awful.widget.tasklist.filter.currenttags,
      buttons = tasklist_buttons
    }
    ltsk_background = wibox.widget {
     separators.arrow_right("alpa", beautiful.bg_normal),
     {
        s.mytasklist,
        bg = beautiful.bg_normal,
        margin = 4,
        widget = wibox.container.background
      },
      separators.arrow_left(beautiful.bg_normal, "alpha"),
      layout = wibox.layout.align.horizontal
    }
    
    --add widgets to the wibox
    s.mywibox:setup {
      layout = wibox.layout.align.horizontal,
      --expand = "none",
      height = 50,
      bg = "#00000000",
      { -- Left widgets
        layout = wibox.layout.fixed.horizontal,
        wibox.container.margin(lch_background, 0, 2),
	wibox.container.margin(ltg_background, 2, 2),
        --wibox.container.margin(s.mypromptbox, 2, 2),
        wibox.container.margin(wea_background, 2, 2),
        wibox.container.margin(mail_background, 2, 2),
        wibox.container.margin(mpd_background, 2, 2),
      },
        wibox.container.margin(ltsk_background, 2, 2), -- Middle widget
      { -- Right widgets
        layout = wibox.layout.fixed.horizontal,
        wibox.container.margin(cpu_background, 2, 2),
        wibox.container.margin(ram_background, 2, 2),
        wibox.container.margin(net_background, 2, 2),
        wibox.container.margin(bat_background, 2, 2),
        wibox.container.margin(vol_background, 2, 2),
        wibox.container.margin(lk_background, 2, 2),
        wibox.container.margin(st_background, 2, 2),
	wibox.container.margin(tc_background, 2, 2),
        wibox.container.margin(lb_background, 2)
      },
    }

    --gears.timer.delayed_call(dock_wibox, s)
    s.quake = lain.util.quake{app = "urxvtc"}

end)

